#version 100

precision highp float;

varying highp vec2 texcoord;

const int max_iterations = 120;
const float cxmin = -2.0;
const float cxmax = 1.0;
const float cymin = -1.5;
const float cymax = 1.5;

const float scale_x = cxmax - cxmin;
const float scale_y = cymax - cymin;

vec2 square_complex(vec2 c) {
    return( vec2(
    c.x*c.x - c.y*c.y,
    2.0 * c.x * c.y
    ));
}

void main() {
    vec2 c = vec2(texcoord.x*scale_x + cxmin, texcoord.y*scale_y + cymin);
    vec2 z = vec2(0.0, 0.0);

    int b = -1;
    for (int i = 0; i < max_iterations; i++) {
        if (z.x*z.x + z.y*z.y > 4.0) {
            b = i;
            break;
        }
        z = square_complex(z) + c;
    }
    if(b == -1) {
        b = max_iterations;
    }
    float intensity = float(b)/float(max_iterations);
    intensity = 2.0*intensity / (abs(intensity) + 1.0);
    float r = max(0.0, 2.0*intensity - 1.0);

    gl_FragColor = vec4(r, intensity, intensity, 1.0);
}