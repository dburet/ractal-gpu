#version 460
#extension GL_NV_gpu_shader_fp64 : enable

layout(location = 0) out vec4 FragColor;




//varying highp vec2 texcoord;
layout(location = 0) in vec2 texcoord;

// replaced by default var in newer versions of glsl
// in vec4 gl_FragCoord; // default input
in vec4 gl_FragCoord;

const int max_iterations = 255;
const double cxmin = -2.0;
const double cxmax = 1.0;
const double cymin = -1.5;
const double cymax = 1.5;

const double scale_x = cxmax - cxmin;
const double scale_y = cymax - cymin;

dvec2 square_complex(dvec2 c) {
    return (dvec2(
    c.x*c.x - c.y*c.y,
    2.0 * c.x * c.y
    ));
}

double SIC(int p, int N, int M, vec2 z) {
    // smooth iteration count, see http://jussiharkonen.com/gallery/coloring-techniques/
    // p is the max degree of the polynom used in our julia set definition
    // N is iteration when reaching bailout value
    // M is bailout value

    float r = float(length(z));
    return double(float(N) + 1.0 + (log((log(float(M))) / log(r)) / log(float(p))));
}

vec4 colorize(float index) {
    float seuil1=0.300;
    float seuil2=0.400;
    float seuil3=0.500;
    vec3 color;

    vec3 grey  = vec3(180.0/255.0, 212.0/255.0, 229.0/255.0);
    vec3 dark  = vec3(40.0/255.0, 48.0/255.0, 64.0/255.0);
    vec3 yellow= vec3(192.0/255.0, 192.0/255.0, 18.0/255.0);
    vec3 orange  = vec3(192.0/255.0, 25.0/255.0, 25.0/255.0);
    vec3 red  = vec3(64.0/255.0, 0.0/255.0, 0.0/255.0);

    index = index / float(max_iterations);
    //4 levels color shading max_iterations

    if (index < seuil1) { color  = mix (grey, dark, smoothstep(0.0, seuil1, index)); }
    else if (index < seuil2) { color  = mix (dark, yellow, smoothstep(seuil1, seuil2, index)); }
    else if (index < seuil3) { color  = mix (yellow, orange, smoothstep(seuil2, seuil3, index)); }
    else color  = mix (orange, red, smoothstep(seuil3, 1.0, index));

    return vec4(color, 1.0);
}

void main() {
    //  vec2 c = vec2(texcoord.x*scale_x + cxmin, texcoord.y*scale_y + cymin);

    // gl_fragCoord seems to be in pixel 0,0 being lower left corner
    // TODO: get max
    // TODO: get zoom level and center from rust code

    //dvec2 c = dvec2(gl_FragCoord.x/600.0-1.5, gl_FragCoord.y/600.0-0.5);
    dvec2 c = dvec2(texcoord.x*scale_x + cxmin, texcoord.y*scale_y + cymin);
    dvec2 z = dvec2(0.0, 0.0);

    int b = -1;
    for (int i = 0; i < max_iterations; i++) {
        if (z.x*z.x + z.y*z.y > 4.0) {
            b = i;
            break;
        }
        z = square_complex(z) + c;
    }
    if (b == -1) {
        FragColor = vec4(0, 0, 0, 1.0);
    } else {

        float mu = float(b) - log(log( float(length(z)) )) / log(2.0);

        //if mu> maxmu {maxmu=mu;};
        //palette[mu as usize];
        FragColor = colorize(mu);
    }
    //debug
    //FragColor = colorize(gl_FragCoord.y+0.5);

    vec3 black  = vec3( 0.0, 0.0, 0.0);
    vec3 white  = vec3( 1.0, 1.0, 1.0);

    //FragColor =  vec4(mix(black,white,gl_FragCoord.y/600.0),1.0);
}